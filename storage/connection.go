package storage

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/lib/pq" // load postgres dirver
)

var (
	// DBName Database name
	DBName = "project"
	// DBUser Database user name
	DBUser = "default"
	// DBPassword Database password
	DBPassword = "secret"
	// DBPort Port for database connection
	DBPort = "5432"
	// DBHost Hostname for database or IP
	DBHost = "localhost"
	// DBSSLMode SSL mode for connection
	DBSSLMode = "disable"
)

// DB instance
func DB() *sql.DB {
	resetVariables()
	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s", DBHost, DBPort, DBUser, DBPassword, DBName, DBSSLMode)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}
	return db
}

func resetVariables() {
	if len(os.Getenv("DB_NAME")) > 0 {
		DBName = os.Getenv("DB_NAME")
	}

	if len(os.Getenv("DB_USER")) > 0 {
		DBUser = os.Getenv("DB_USER")
	}

	if len(os.Getenv("DB_PASSWORD")) > 0 {
		DBPassword = os.Getenv("DB_PASSWORD")
	}

	if len(os.Getenv("DB_PORT")) > 0 {
		DBPort = os.Getenv("DB_PORT")
	}

	if len(os.Getenv("DB_HOST")) > 0 {
		os.Getenv("DB_HOST")
	}

	if len(os.Getenv("DB_SSL_MODE")) > 0 {
		DBSSLMode = os.Getenv("DB_SSL_MODE")
	}
}
