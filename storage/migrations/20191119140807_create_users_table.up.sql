CREATE TABLE IF NOT EXISTS users (
    id         SERIAL PRIMARY KEY NOT NULL,
    first_name VARCHAR (50),
    last_name  VARCHAR (50),
    email      VARCHAR (50),
    password   VARCHAR (50),
    created_at timestamp DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP
);
