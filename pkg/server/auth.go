package server

import (
	"encoding/json"
	"net/http"
	"template/pkg/users"
)

// Register ...
func (ctx *Context) Register(w http.ResponseWriter, r *http.Request) {
	user := &users.User{}
	err := user.Create("John", "Doe", "john@app.com", "password")
	if err != nil {
		panic(err)
	}
	data, _ := json.Marshal("ok")
	w.Write(data)
}
