package server

import (
	"net/http"
)

// Context server wrapper
type Context struct {
	Router *http.ServeMux
}

// Routes ...
func (ctx *Context) Routes() {
	ctx.Router.Handle("/auth/register", http.HandlerFunc(ctx.Register))
}
