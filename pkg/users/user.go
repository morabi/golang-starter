package users

import "template/storage"

// User model
type User struct {
	ID        string
	FirstName string
	LastName  string
	Email     string
}

// Create User
func (u *User) Create(firstName string, lastName string, email string, password string) error {
	db := storage.DB()
	defer db.Close()
	_, err := db.Exec(`INSERT INTO users (first_name, last_name, email, password) values ($1, $2, $3, $4)`, firstName, lastName, email, password)
	return err
}

// Find User
func (u *User) Find(ID string) (*User, error) {
	return nil, nil
}

// FindByEmail User
func (u *User) FindByEmail(email string) (*User, error) {
	return nil, nil
}
