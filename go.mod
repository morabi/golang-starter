module template

go 1.12

require (
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.2.0
	github.com/spf13/cobra v0.0.5
)
