package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate"
	_ "github.com/golang-migrate/migrate/database/postgres" // Load postgres driver
	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"
	"github.com/spf13/cobra"
)

var (
	// DBName Database name
	DBName = "project"
	// DBUser Database user name
	DBUser = "default"
	// DBPassword Database password
	DBPassword = "secret"
	// DBPort Port for database connection
	DBPort = "5432"
	// DBHost Hostname for database or IP
	DBHost = "localhost"
	// DBSSLMode SSL mode for connection
	DBSSLMode = "disable"
	steps     int
	down      bool
	migrator  *migrate.Migrate
)

// migrateCmd represents the migrate command
var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "",
	Long: ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Println("migrate called")
		if down {
			return migrationDown()
		}
		return migrationUp()
	},
}

func init() {
	rootCmd.AddCommand(migrateCmd)
	migrateCmd.Flags().IntVarP(&steps, "steps", "s", 2, "Steps for migrations")
	migrateCmd.Flags().BoolVarP(&down, "down", "d", false, "Drop migrations")

	resetVariables()
	migrationInit()
}

func migrationInit() error {

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	path := fmt.Sprintf("file://%s/storage/migrations", dir)
    dbinfo := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=%s", DBUser, DBPassword, DBHost, DBPort, DBName, DBSSLMode)
	migrator, err = migrate.New(path, dbinfo)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func migrationUp() error {
	fmt.Println("up")
	v, d, err := migrator.Version()
	if err != nil {
		fmt.Println(err)
	}

	if d {
		if err := migrator.Force(int(v)); err != nil {
			log.Fatal(err)
			return err
		}
	} else {
		if err := migrator.Up(); err != nil {
			log.Fatal(err)
			return err
		}
	}
	return nil
}

func migrationDown() error {
	fmt.Println("down")
	if err := migrator.Down(); err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

func resetVariables() {
	if len(os.Getenv("DB_NAME")) > 0 {
		DBName = os.Getenv("DB_NAME")
	}

	if len(os.Getenv("DB_USER")) > 0 {
		DBUser = os.Getenv("DB_USER")
	}

	if len(os.Getenv("DB_PASSWORD")) > 0 {
		DBPassword = os.Getenv("DB_PASSWORD")
	}

	if len(os.Getenv("DB_PORT")) > 0 {
		DBPort = os.Getenv("DB_PORT")
	}

	if len(os.Getenv("DB_HOST")) > 0 {
		os.Getenv("DB_HOST")
	}

	if len(os.Getenv("DB_SSL_MODE")) > 0 {
		DBSSLMode = os.Getenv("DB_SSL_MODE")
	}
}
